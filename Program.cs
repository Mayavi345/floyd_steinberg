﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using ImageMagick;
using System.IO;

namespace Floyd_Steinberg
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string fileName = "CustomSize.jpg";
            string filePath = @"C:\Users\jack.hsu\source\repos\WindowsForms Ex01\Floyd_Steinberg\"+ fileName;
            
            //輸出檔案去掉副檔名
            string[] sArray = fileName.Split('.');
            string convertFilePath = @"C:\Users\jack.hsu\source\repos\WindowsForms Ex01\Floyd_Steinberg\"+ sArray [0]+ "_convert.png";
        
   

            using (MemoryStream ms = new MemoryStream())
            {
                //輸出解析度設定
                int width = 296;
                int heigh = 152;

                Image fileImage = Image.FromFile(filePath);

                //1.重新設定大小
                //2.ImageFormat設定Png會導致輸出位元錯誤
                ResizeTo(fileImage, width, heigh).Save(ms, ImageFormat.Jpeg);
                ms.Position = 0;
             
                using (var image = new MagickImage(ms))
                {
                   //抖色模式
                    DitherMethod DM = DitherMethod.FloydSteinberg;
      

                    MagickColor[] myEntries = new MagickColor[3];
                    myEntries[0] = MagickColor.FromRgb(255, 255, 255);
                    myEntries[1] = MagickColor.FromRgb(255, 0, 0);
                    myEntries[2] = MagickColor.FromRgb(0, 0, 0);

                    //抖色以及品質設定
                    QuantizeSettings settings = new QuantizeSettings
                    {
                        Colors = 3,
                        TreeDepth = 2,
                        DitherMethod = DM,
                    };
                    //指定色板取得相近色
                    image.Map(myEntries, settings);

                    //設定色深
                    image.Depth = 2;
                    image.Write(convertFilePath);

                 }

            }

        }
        internal static Image ResizeTo(Image sourceImage, int width, int height)
        {
            System.Drawing.Image newImage = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.DrawImage(sourceImage, new Rectangle(0, 0, width, height));
                gr.Dispose();
            }
            return newImage;
        }

    }
}
